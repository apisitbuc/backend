import express from "express";
import { connectDatabase } from "./database";
import routes from "./routes";
import cors from "cors";

const PORT = 8000;

const app = express();
app.use(cors());
app.use(express.json());
app.use(routes);

connectDatabase()
  .then(() => {
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch((error) => {
    console.error("Error starting server:", error);
  });
