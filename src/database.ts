import { createConnection } from "typeorm";

export const connectDatabase = async () => {
  try {
    await createConnection();
    console.log("Connected to PostgreSQL!");
  } catch (error) {
    console.error("Error connecting to PostgreSQL:", error);
  }
};