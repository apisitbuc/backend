import { Request, Response } from "express";
import { getRepository } from "typeorm";
import { HomeEntity, IHome } from "../entities/home";

const houses: IHome[] = [];

export const createHome = async (req: Request, res: Response) => {
  try {
    // const homeRepository = getRepository(HomeEntity);
    const { name, desc, price, post_code } = req.body;

    const home = new HomeEntity();
    home.id = Math.floor(Math.random() * (999 - 100 + 1) + 100);
    home.name = name;
    home.desc = desc;
    home.price = parseFloat(price);
    home.post_code = post_code;

    // await homeRepository.save(home);

    houses.push(home);

    res.status(201).json({ message: "Home created successfully" });
  } catch (error) {
    console.error("Error creating home:", error);
    res.status(500).json({ error: "Failed to create home" });
  }
};

export const getHomes = async (req: Request, res: Response) => {
  try {
    const { skip, take } = req.query as { skip: string; take: string };
    // const homeRepository = getRepository(HomeEntity);

    // const [homes, count] = await homeRepository.findAndCount({
    //   skip: parseInt(skip),
    //   take: parseInt(take),
    // });

    res.json({ homes: houses, count: houses.length });
  } catch (error) {
    console.error("Error fetching homes:", error);
    res.status(500).json({ error: "Failed to fetch homes" });
  }
};

export const getPostCodes = async (req: Request, res: Response) => {
  try {
    // const homeRepository = getRepository(HomeEntity);
    // const postCodes = await homeRepository
    //   .createQueryBuilder("home")
    //   .select("home.postCode")
    //   .distinct(true)
    //   .getRawMany();

    res.json({ payload: houses.map((house) => house.post_code) });
  } catch (error) {
    console.error("Error fetching post codes:", error);
    res.status(500).json({ error: "Failed to fetch post codes" });
  }
};

export const getPostCodeStats = async (req: Request, res: Response) => {
  try {
    const { id } = req.params;
    const homeRepository = getRepository(HomeEntity);
    const postCodeStats = await homeRepository
      .createQueryBuilder("home")
      .select("home.postCode")
      .addSelect("AVG(home.price)", "avgPrice")
      .addSelect(
        "PERCENTILE_CONT(0.5) WITHIN GROUP (ORDER BY home.price)",
        "medianPrice"
      )
      .where("home.postCode = :id", { id })
      .getRawOne();

    res.json({ payload: postCodeStats });
  } catch (error) {
    console.error("Error fetching post code stats:", error);
    res.status(500).json({ error: "Failed to fetch post code stats" });
  }
};
