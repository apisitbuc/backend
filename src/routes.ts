import { Router } from "express";
import { createHome, getHomes, getPostCodeStats, getPostCodes } from "./controllers/homeController";


const router = Router();

router.post("/home", createHome);
router.get("/home", getHomes);
router.get("/postCode", getPostCodes);
router.get("/postCode/:id", getPostCodeStats);

export default router;
