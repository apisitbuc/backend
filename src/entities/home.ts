import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class HomeEntity implements IHome {
  @PrimaryGeneratedColumn()
  id!: number;

  @Column()
  name?: string;

  @Column()
  desc?: string;

  @Column({ type: "numeric", precision: 10, scale: 2 })
  price?: number;

  @Column()
  post_code?: string;
}

export interface IHome {
  id: number;
  name?: string;
  desc?: string;
  price?: number;
  post_code?: string;
}
